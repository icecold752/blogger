class ArticlesController < ApplicationController
include ArticlesHelper

  def index
    @articles = Article.all
  end
  def show
    @article = Article.find(params[:id])
    @comment = Comment.new
    @comment.article_id = @article.id
  end
  def new
    @article = Article.new
  end
  def destroy
    @article = Article.find(params[:id])
    @article.destroy
    flash.notice = "Article '#{@article.title}' is deleted forever!"
    redirect_to articles_path
  end

  def edit
    @article = Article.find(params[:id])
  end

  def update
      @article = Article.find(params[:id])
      @article.update(article_params)

      flash.notice = "Article '#{@article.title}' Updated!"

      redirect_to article_path(@article)
  end

  def create
    @article = Article.new(article_params)
#      title: params[:article][:title],
#      body: params[:article][:body],
#     published:  params[:article][:published]
#        )
    @article.save
    flash.notice = "Article '#{@article.title}'is listed!"
    redirect_to article_path(@article)
  end

  private
    def article_params
      params.require(:article).permit(:title, :body, :tag_list, :published, :image)
    end
end
