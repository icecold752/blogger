module ArticlesHelper
  def article_params
      params.require(:article).permit(:title, :body, :published, :tag_list, :image)
    end
end
